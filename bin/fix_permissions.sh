#!/usr/bin/env sh

docker exec -it sf2_php_1 sh -c "chown -R www-data:www-data ./app ./src ./vendor"
docker exec -it sf2_php_1 sh -c "chmod -R 755 ./app/logs"
