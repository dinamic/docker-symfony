#!/usr/bin/env sh

docker exec -it sf2_php_1 sh -c "[ -e /usr/local/bin/rocketeer ] || apk add wget"
docker exec -it sf2_php_1 sh -c "[ -e /usr/local/bin/rocketeer ] || wget http://rocketeer.autopergamene.eu/versions/rocketeer.phar"
docker exec -it sf2_php_1 sh -c "[ -e /usr/local/bin/rocketeer ] || chmod +x rocketeer.phar"
docker exec -it sf2_php_1 sh -c "[ -e /usr/local/bin/rocketeer ] || mv rocketeer.phar /usr/local/bin/rocketeer"
docker exec -it sf2_php_1 su www-data -s /bin/sh -c "rocketeer $@"
