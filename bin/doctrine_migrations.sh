#!/usr/bin/env sh

docker exec -it sf2_php_1 su www-data -s /bin/sh -c "php app/console doctrine:migrations:migrate"
