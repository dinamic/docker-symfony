#!/usr/bin/env sh

docker inspect --format '{{ .NetworkSettings.Networks.docker_default.IPAddress }}' sf2_$@_1
